#!/usr/bin/env bash

set -euo pipefail

TOOL_NAME="calc"
TOOL_TEST="calc"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

inst_version() {
  local base="`apt-cache policy libreoffice-calc | head -2 | tail -1 | sed -e 's/ //g' | cut -d ":" -f3 | cut -d "-" -f1`" > /dev/null
  if [ "$base" == "" ]; then
   return 0
  fi
  echo "`apt-cache policy libreoffice-calc | head -2 | tail -1 | sed -e 's/ //g' | cut -d ":" -f3 | cut -d "-" -f1`"
}

avail_version() {
  echo "`apt-cache policy libreoffice-calc | head -3 | tail -1 | sed -e 's/ //g' | cut -d ":" -f3 | cut -d "-" -f1 | cut -d "~" -f1`"
}

is_installed() {
  apt list --installed 2>/dev/null | grep "libreoffice-calc/"
}

list_all_versions() {
  iv=`inst_version`
  av=`avail_version`
  if [[ "${iv}" == "${av}" ]]; then
    echo "$iv"
  else
    echo "$iv"
    echo "$av"
  fi
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"
  local avail_v="`avail_version`"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    echo "Available version: $avail_v"
    calc_installed="`is_installed`"
    if [ ! -z "$calc_installed" ] && [ "$calc_installed" != "" ]; then
      calc_v="`inst_version`"
      echo "Installed version: $calc_v"
      if [[ ! "$calc_v" =~ "$avail_v" ]]; then
        echo "Upgrading.."
  
        sudo apt -y --only-upgrade install libreoffice*
      fi
    else
      echo "Not installed, installing.."

      sudo apt install -y libreoffice-calc libreoffice-gtk3
    fi
    mkdir -p "$install_path/bin"
    touch "$install_path/bin/calc"
    chmod a+x "$install_path/bin/calc"
    echo "/usr/bin/libreoffice --calc \"\$@\"" >> $install_path/bin/calc

    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
  if [ -z ${DAT_CUBIC+x} ] && [ -f /usr/share/applications/libreoffice-calc.desktop ]; then
    sudo rm /usr/share/applications/libreoffice-calc.desktop
  fi
  exit 0
}
